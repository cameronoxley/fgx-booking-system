SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `FGX` ;
CREATE SCHEMA IF NOT EXISTS `FGX` ;
USE `FGX` ;

-- -----------------------------------------------------
-- Table `FGX`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FGX`.`user` ;

CREATE TABLE IF NOT EXISTS `FGX`.`user` (
  `id` VARCHAR(32) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(72) NOT NULL,
  `last_login` DATETIME NOT NULL,
  `type` ENUM('user', 'admin') NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FGX`.`car`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FGX`.`car` ;

CREATE TABLE IF NOT EXISTS `FGX`.`car` (
  `id` VARCHAR(32) NOT NULL,
  `kms` INT NULL,
  `make` VARCHAR(45) NULL,
  `model` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FGX`.`booking`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FGX`.`booking` ;

CREATE TABLE IF NOT EXISTS `FGX`.`booking` (
  `id` VARCHAR(32) NOT NULL,
  `user_id` VARCHAR(32) NOT NULL,
  `car_id` VARCHAR(32) NOT NULL,
  `description` VARCHAR(45) NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_booking_users_idx` (`user_id` ASC),
  INDEX `fk_booking_car1_idx` (`car_id` ASC),
  CONSTRAINT `fk_booking_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `FGX`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `FGX`.`car` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `FGX`.`user_session`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `FGX`.`user_session` ;

CREATE TABLE IF NOT EXISTS `FGX`.`user_session` (
  `id` VARCHAR(32) NOT NULL,
  `user_id` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_session_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_session_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `FGX`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
