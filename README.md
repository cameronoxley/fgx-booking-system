# Scenario. #
Company car booking scheme.

Users need to access a web page that they will have access to via username and
password.

Once given access to the page they will see a calendar (monthly) for the
company car.

If they want to book the company car for a day they will be able to see if it is booked and
by who. If it is not booked they will be able to book it. In the booking form they will need
to provide the time that they will be booking it for and provide a reason. There will be a
place to put in the KM recorded.

If the user wants to cancel the booking they can only select a booking that they have
made, and they cannot cancel a booking or delete any information that has occurred in the past.

If a booking is made or cancelled an email is sent to the user and the administrator informing them of
the event.

# Notes: #
Wherever possible I've tried to use the experience I've gained to create the system, purely because I didn't want a misguided idea of my capabilities. The implementation is going to be more of a raw "this is what I can currently do" as opposed to "this is what I've googled and made". Having said that, I've put a URL reference wherever I've used an external helper function.

* The requirement didn't speak of the the creation or management of user logins and or passwords, I've assumed then that this will be handled by an external entity/system. Should this become a requirement, I am happy to discuss my implementation plan.

* At the moment I've decided to create the system with only 1 car in mind, this could easily be altered to have more than 1 company car, thereby allocating dates and times per car to a user.

* The requirement also spoke of "Administrators" as users, thereby inferring that there is a permissions model. I've created a basic concept with regard to "Submenus" that would allow for administrator menus to be loaded separately from users', however this is a very simplistic approach and a more appropriate model would be to have a separate "Permissions" table defining what roles are linked to what capabilities on the system, however I feel that this is not within the scope of the project.

* Regarding the Authentication model, I've created utilised standard approach, I've placed comments within the code showing further implementation plans and ideas.
