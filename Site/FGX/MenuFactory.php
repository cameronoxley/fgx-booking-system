<?php
namespace FGX;
use FGX\Menu\AdminMenu;
use FGX\Menu\UserMenu;

/**
 * Class Menu
 * @package FGX
 */
class MenuFactory {

	/**
	 * Returns the menu type based on the menu specifieds
	 * @param User $user
	 * @return AdminMenu|UserMenu
	 */
	public static function getMenu(User $user) {
		switch ($user->getType())
		{
			case 'admin' :
				return new AdminMenu();
				break;
			case 'user' :
				return new UserMenu();
				break;
		}
	}
}
