<?php

namespace FGX\Menu;


use FGX\User;

class AdminMenu implements Menu {

	function getHeader(User $user)
	{
		return "
		<!--sidebar start-->
		<aside>
          <div id='sidebar'  class='nav-collapse '>
              <!-- sidebar menu start-->
              <ul class='sidebar-menu' id='nav-accordion'>
              	  <h5 class='centered'>".$user->getUsername()."</h5>
		";
	}

	function getContent(User $user)
	{
		return "
			  <li class='sub-menu'>
				  <a href='javascript:;' >
					  <i class='fa fa-th'></i>
					  <span>Bookings</span>
				  </a>
				  <ul class='sub'>
					  <li><a class='fa fa-calendar' href='general.html'> View</a></li>
					  <li><a class='fa fa-asterisk' href='buttons.html'> Create</a></li>
					  <li><a class='fa fa-times' href='panels.html'> Delete</a></li>
				  </ul>
			  </li>
		";
	}

	function getFooter(User $user)
	{
		return "
			  </ul>
				  <!-- sidebar menu end-->
		  </div>
	    </aside>
		<!--sidebar end-->
		";
	}
}