<?php

namespace FGX\Menu;
use FGX\User;

interface Menu {
	/**
	 * Returns the header html for the menu
	 * @param User $user
	 * @return mixed
	 */
	function getHeader(User $user);

	/**
	 * Returns the content and any submenus
	 * @param User $user
	 * @return mixed
	 */
	function getContent(User $user);

	/**
	 * Returns the menu footer
	 * @param User $user
	 * @return mixed
	 */
	function getFooter(User $user);
} 