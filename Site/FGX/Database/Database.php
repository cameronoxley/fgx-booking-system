<?php

namespace FGX\Database;
use FGX\Error;

require_once('assets/includes/config.inc.php');
require_once('assets/includes/session.inc.php');

class Database {

	/**
	 * Uses the MySQLI Persistent connection interface to establish a conenction to the DB
	 * @param null $username
	 * @param null $password
	 * @param null $database
	 * @param null $host
	 * @param null $port
	 * @return Error
	 */
	static function connect($username=null, $password=null, $database=null, $host=null, $port=null) {

		//Use default config values, unless they are explicitly passed
		$username = (isset($username) ? $username : \Config::$database['username']);
		$password = (isset($password) ? $password : \Config::$database['password']);
		$database = (isset($database) ? $database : \Config::$database['database']);
		$host = (isset($host) ? $host : \Config::$database['host']);
		$port = (isset($port) ? $port : \Config::$database['port']);

		$mysqli = new \mysqli($host, $username, $password, $database, $port);

		if ($mysqli->connect_errno) {
			return new Error("An Error has occurred, please try again in 5 minutes", "MySQL Failed to connect->(" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
		}

		return $mysqli;
	}
} 