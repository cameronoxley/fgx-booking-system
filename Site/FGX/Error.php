<?php

namespace FGX;

/*
 * Set errors to be logged in the php-error.log file.
 * Note: this will override the php.ini file
 */
ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");

/**
 * Class responsible for the passing and logging of errors
 * Class Error
 * @package FGX
 */
class Error {
	private $message = "";
	private $devMessage = "";

	/**
	 * @param $message
	 * @param null $devMessage
	 */
	function __construct($message, $devMessage=null) {
		$this->message = $message;
		$this->devMessage = $devMessage;
		$this->logError($message, $devMessage);
	}

	/**
	 * Method for logging
	 * @param $message
	 * @param $devMessage
	 */
	private function logError($message, $devMessage ) {
		error_log($message . " ( $devMessage )");
	}

	/**
	 * Returns the error message
	 * @return mixed
	 */
	public function getErrorMessage() {
		return $this->message;
	}

	/**
	 * Returns the Dev message
	 * @return mixed
	 */
	public function getDevMessage() {
		return $this->devMessage;
	}
} 