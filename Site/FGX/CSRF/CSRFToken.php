<?php

namespace FGX\CSRF;

use FGX\Error;
use FGX\UtilitiesTrait;

class CSRFToken {
	use UtilitiesTrait;

	/**
	 * Returns a CSRF token as an HTML input
	 * @return string
	 */
	public function getToken() {
		$token = $this->getUUID();
		$this->updateSession($token);
		return "<input type='hidden' name='csrf_token' value='$token'/>";
	}

	/**
	 * Validate the given token against the Session token to check for a CSRF attack
	 * @param $token
	 * @return bool|Error
	 */
	public function checkToken($token) {
		if (isset($_SESSION['csrf_token']) && $_SESSION['csrf_token'] == $token) {
			return TRUE;
		} else {
			//Technically this would be an excellent use of a langage file
			$error = new Error("Sorry, something has gone wrong, please try again", "Token mismatch, possible CSRF attack");
		}
	}

	/**
	 * Update the session with the CSRF token fot the other side of the form
	 * @param $uuid
	 */
	private function updateSession($uuid) {
		$_SESSION['csrf_token'] = $uuid;
	}
} 