<?php
session_start();
session_unset(); // Unset all of the session variables.
session_destroy(); // Destroy the session.
session_write_close(); //Write the destroyed data
session_regenerate_id(true); //Update the session with a new ID
header("Location: /login.php"); //Redirect to login
exit;