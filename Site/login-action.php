<?php
use FGX\Database\Database;
use \FGX\Error;
use \FGX\CSRF\CSRFToken;

	require_once('assets/includes/session.inc.php');

	//Check CSRF Token
	if (isset($_POST['csrf_token']) && $_POST['csrf_token'] != "") {
		$csrf = new CSRFToken();
		$failed = $csrf->checkToken($_POST['csrf_token']);

		//Return the error
		if (is_a($failed, 'Error')) {
			return $failed;
		}
	}

	//Check that we've received all the data requested
	$username = isset($_POST['username']) ? $_POST['username'] : "";
	$password = isset($_POST['password']) ? $_POST['password'] : "";

	if ($username == "" || $password == "") {
		return new Error("Please fill in all the fields",  "Failed validation, username or password was null");
	}

	//Connect to DB for next phase of validation
	$connection = Database::connect();

	//Return the error
	if (is_a($connection, 'Error')) {
		return $connection;
	}

	//Use prepared statement to prevent injection, fetch username
	$connection->prepare('SELECT id, password, type FROM user WHERE username = ?');
	$statement->bind_param('s', $username);

	$statement->execute();
	$statement->store_result();

	//Retrieve result and bind to variables
	$statement->bind_result($userID, $dbPassword, $type);
	$statement->fetch();

	//Check if user exists on the system
	if ($stmt->num_rows == 1) {

		//Use password_verify to check encrypted password against raw text.
		//Note: A better way to do this is to actually SHA1 encrypt the password on the user's side (in javascript) so that it is never passed around in raw text
		if (password_verify($password, $dbPassword) === TRUE) {

			//Bring Utilities in for global use
			$util = new \FGX\Utilities();

			//Set session requirements
			$_SESSION['username'] = $username;
			$_SESSION['type'] = $username;
			$_SESSION['logged_in'] = $util->getUUID();

			//Insert into the user_session table for checks later on
			$connection->prepare('INSERT INTO user_session (id, user_id) VALUES (?, ?)');
			$statement->bind_param('s', $_SESSION['logged_in']);
			$statement->bind_param('s', $username);
			$statement->execute();

		} else {
			//Technically we should log this request with its IP for vertical and horizontal attacks
			return new Error("Username/Password is incorrect", "Password Validation Failed");
		}

	} else {
		//Lets be vague as to what the problem actually is
		return new Error("Username/Password is incorrect", "Username: '$username' does not exist");
	}

	if ($fail === FALSE) {
		return new Error("Please try again", "Failed Vaidation on Login", $fail);
	}
