<?php
class Config {
	static public $client = 'FGX Booking System';
	static public $database = [
		'host' => 'localhost',
		'database' => 'FGX',
		'username' => 'root',
		'password' => 'root',
		'port' => '8889'
	];
}