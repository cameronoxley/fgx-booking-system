<?php
	session_start();
	require_once('assets/includes/header.inc.php');
?>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">

			<?php
				$error = (isset($_GET['error'])? $_GET['error'] : 'An error has occurred.');
			?>

			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="showback">
					<h4><i class="fa fa-times"></i> Error</h4>
					<div class="alert alert-danger"><b>Error!</b> <?= $error ?>.</div>
				</div><!-- /showback -->

			</div>

	  	</div>
	  </div>

<?php
	require_once('assets/includes/scripts.inc.php');
?>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


<?php
	require_once('assets/includes/footer.inc.php');
?>
