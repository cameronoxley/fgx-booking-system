<?php
	session_start();
	require_once('assets/includes/header.inc.php');
?>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" action="login-action.php" method="post">
		        <h2 class="form-login-heading">sign in</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" placeholder="User ID" name="username" autofocus>
		            <br>
		            <input type="password" class="form-control" name="password" placeholder="Password">

					<?php
						$csrf = new \FGX\CSRF\CSRFToken();
						echo $csrf->getToken();
					?>
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.php#myModal"> Forgot Password?</a>
		
		                </span>
		            </label>
		            <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
		        </div>
		
		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="ForgotPassword" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Please contact the Administrator to reset your password.</p>
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->
		      </form>
	  	</div>
	  </div>

<?php
	require_once('assets/includes/scripts.inc.php');
?>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


<?php
	require_once('assets/includes/footer.inc.php');
?>
